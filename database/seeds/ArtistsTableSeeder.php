<?php

use Illuminate\Database\Seeder;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artists')->insert([[
            'id' =>'1',
            'name' => 'James',
            'firstname' => 'Lily',
            'birthdate' => 1939,
            'image' => 'profile_1.png',
        ],[
            'id' =>'2',
            'name' => 'Bale',
            'firstname' => 'Christian ',
            'birthdate' => 1946,
            'image' => 'profile_2.png',
        ],[
            'id' =>'3',
            'name' => 'Roberts ',
            'firstname' => 'Julia',
            'birthdate' => 1946,
            'image' => 'profile_3.png',
        ],[
            'id' =>'4',
            'name' => 'Phoenix ',
            'firstname' => 'Joaquin',
            'birthdate' => 1946,
            'image' => 'profile_4.png',
        ],[
            'id' =>'5',
            'name' => 'Jolie',
            'firstname' => 'Angelina',
            'birthdate' => 1946,
            'image' => 'profile_5.png',
        ],[
            'id' =>'6',
            'name' => 'Pitt ',
            'firstname' => 'Brad',
            'birthdate' => 1946,
            'image' => 'profile_6.png',
        ],[
            'id' =>'7',
            'name' => 'Sy',
            'firstname' => 'Omar',
            'birthdate' => 1946,
            'image' => 'profile_7.png',
        ],[
            'id' =>'8',
            'name' => 'Jon ',
            'firstname' => 'Favreau',
            'birthdate' => 1946,
            'image' => 'profile_8.png',
        ],[
            'id' =>'9',
            'name' => 'Bensetti',
            'firstname' => 'Rayane',
            'birthdate' => 1946,
            'image' => 'profile_9.png',
        ],[
            'id' =>'10',
            'name' => 'Wiggins',
            'firstname' => 'Chris ',
            'birthdate' => 1946,
            'image' => 'profile_10.png',
        ],[
            'id' =>'12',
            'name' => 'Franck	',
            'firstname' => 'Dubosc ',
            'birthdate' => 1946,
            'image' => 'pprofile_12.png',
        ]],);
    }
}
