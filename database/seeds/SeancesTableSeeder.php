<?php

use Illuminate\Database\Seeder;

class SeancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seances')->insert([[
            'title_id' => '1',
            'roomName_id' => '1',
            'hours' => 'Mon 13:00',
        ],[
            'title_id' => '2',
            'roomName_id' => '2',
            'hours' => 'Mon 13:00',
        ],[
            'title_id' => '3',
            'roomName_id' => '3',
            'hours' => 'Mon 13:00',
        ],[
            'title_id' => '4',
            'roomName_id' => '1',
            'hours' => 'Mon 13:00',
        ],[
            'title_id' => '5',
            'roomName_id' => '2',
            'hours' => 'Mon 13:00',
        ],[
            'title_id' => '6 ',
            'roomName_id' => '3',
            'hours' => 'Mon 13:00',
        ],[
            'title_id' => ' 7',
            'roomName_id' => '1',
            'hours' => 'Mon 13:00',
        ]],);
    }
}
