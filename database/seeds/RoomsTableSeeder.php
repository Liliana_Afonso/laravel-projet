<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([[
            'roomName' => 'Rex',
            'numberSeats' => '160',
            'cinema_id' => 1,
        ],[
            'roomName' => 'Sud-40',
            'numberSeats' => '220',
            'cinema_id' => 2,
        ],[
            'roomName' => 'Palais-II',
            'numberSeats' => '380',   
            'cinema_id' => 1,    
        ]],);
    }
}
