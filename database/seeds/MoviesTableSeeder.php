<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([[
            'title' => 'Cendrillon',
            'year' => '2015',
            'artist_id' => '1',
            'id'=>'1',
            'image' => 'poster_1.png',
        ],[
            'title' => 'The Dark Knight Rises.Batman',
            'year' => '2012',
            'artist_id' => '2',
            'id'=>'2',
            'image' => 'poster_2.png',
        ],[
            'title' => 'Blanche Neige',
            'year' => '2012',
            'artist_id' => '3',
            'id'=>'3',
            'image' => 'poster_3.png',
        ],[
            'title' => 'Joker 2',
            'year' => '2019',
            'artist_id' => '4',
            'id'=>'4',
            'image' => 'poster_4.png',
        ],[
            'title' => 'Maléfique',
            'year' => '2019',
            'artist_id' => '5',
            'id'=>'5',
            'image' => 'poster_5.png',
        ],[
            'title' => ' Meet Jo Black',
            'year' => '1998',
            'artist_id' => '6',
            'id'=>'6',
            'image' => 'poster_6.png',
        ],[
            'title' => 'Le prince oublié',
            'year' => '2020',
            'artist_id' => '7',
            'id'=>'7',
            'image' => 'poster_7.png',
        ],[
            'title' => '10 jours sans maman',
            'year' => '2020',
            'artist_id' => '12',
            'id'=>'8',
            'image' => 'poster_8.png',
        ],[
            'title' => 'Le Roi Lion',
            'year' => '2019',
            'artist_id' => '9',
            'id'=>'9',
            'image' => 'poster_9.png',
        ],[
            'title' => 'Babar1',
            'year' => '1999',
            'artist_id' => '10',
            'id'=>'10',
            'image' => 'poster_10.png',
        ],[
            'title' => 'Babar2',
            'year' => '1999',
            'artist_id' => '10',
            'id'=>'11',
            'image' => 'poster_11.png',
        ]],);
    }
}
