<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insert([[
            'name' => 'Pathé',
            'street' => 'Luis-Casaï 27',
            'postcode' => 1211,
            'city' => 'Genève',
            'country' => 'Suisse',
           
        ],[
            'name' => 'Aréna Cinema ',
            'street' => 'Route des jeunes 10',
            'postcode' => 1212,
            'city' => 'Lancy',
            'country' => 'Suisse',
        ]],);

       
    }
}
