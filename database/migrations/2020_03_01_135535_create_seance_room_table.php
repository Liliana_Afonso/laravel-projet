<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeanceRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seance_room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role_name', 20);
            // $table->bigInteger('title_id')->unsigned();
            // $table->foreign('title_id')->references('id')->on('seances');
            // $table->bigInteger('artist_id')->unsigned();
            // $table->foreign('artist_id')->references('id')->on('artists');
            $table->bigInteger('roomName_id')->unsigned();
            $table->foreign('roomName_id')->references('id')->on('rooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seance_room');
    }
}
