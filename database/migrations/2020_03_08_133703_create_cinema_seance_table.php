<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCinemaSeanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cinema_seance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role_name', 20);
            $table->bigInteger('title_id')->unsigned();
            $table->foreign('title_id')->references('id')->on('seances');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cinema_seance');
    }
}
