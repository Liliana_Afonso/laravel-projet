<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToSeancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    //pour la migration  des foreign key dans la base de données
    {
        Schema::table('seances', function (Blueprint $table) {
            $table->unsignedBigInteger('title_id');
            $table->foreign('title_id')->references('id')->on('movies');
            $table->unsignedBigInteger('roomName_id');
            $table->foreign('roomName_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seances', function (Blueprint $table) {
            //
        });
    }
}
