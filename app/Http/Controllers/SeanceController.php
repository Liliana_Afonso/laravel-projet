<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Movie;
use \App\Models\Seance;
use \App\Models\Room;
use \App\Http\Requests\SeanceRequest;




class SeanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('seances.index', ['seances' => Seance::paginate(4)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'seances.create', ['movies' => Movie::all()], ['rooms' => Room::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeanceRequest $request)
    {
      
        Seance::create($request->all());
        return redirect()->route('seance.create')
        ->with('ok', __ ('Seance has been saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seance $seance)
    {
        return view('seances.edit', ['seance' => $seance], ['movies' => Movie::all()], ['rooms' => Room::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( SeanceRequest $request, Seance $seance)
    {
        $seance->update( $request->all() );

        return redirect()->route('seance.index')
                        ->with('ok', __('Seance has been updated') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seance $seance)
    {
        
        $seance->delete();
        return response()->json();
    }
    public function __construct(){
        $this->middleware('ajax')->only('destroy');
    }
}
