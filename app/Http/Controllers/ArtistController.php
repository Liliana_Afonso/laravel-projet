<?php
namespace App\Http\Controllers;
use \App\Models\Artist;
use \App\Http\Requests\ArtistRequest;
use Illuminate\Http\Request;
use \App\Models\Movie;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;



class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Artist::all()); pour débugger
        return view ('artists.index', [ 'artists' => Artist::paginate(4) ]);
    }

    /**---
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArtistRequest $request)
    {
        $artist=Artist::create($request->all());
        $poster = $request->file( 'image') ;
        $filename = 'profile_' . $artist->id . '.' . $poster->guessClientExtension();
        Image::make( $poster )->fit( 180, 240)
        ->save( public_path( '/uploads/profiles/' . $filename ) );

        return redirect()->route('artist.create')
        ->with('ok', __ ('Artist has been saved'));
       
    }
  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    {
        // VOICI TOUS NOS TESTS POUR LE LIEN DU MANY
        // $data = DB::table('artists')
        // ->join('movies', 'artists.id', '=', 'movies.artist_id')
        // ->select('movies.title', 'movies.year', 'movies.artist_id')
        // ->get();

        // foreach($data as $movie){
        //     // echo '<li>' . $movie->title . ' ' . $movie->year . ' </li>';

        //     echo '<li value="' . $artist->id . '">' . $movie->artist_id . ' </li>';
        // }

        // return view('artists.show',['artist' => $artist], [ 'movies' => Movie::all()])->with('data', $data);

        return view('artists.show',['artist' => $artist], [ 'movies' => Movie::all()]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        return view('artists.edit', ['artist' => $artist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArtistRequest $request, Artist $artist) // passe d'abord dans artistRequest puis si c'est bon fait resquet et après on dit qu'on update l'artiste selectionné
    {
        $artist->update( $request->all() );

        return redirect()->route('artist.index')
                        ->with('ok', __('Artist has been updated') );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Artist $artist)
    {
        $artist->delete();
        return response()->json();
    }

    public function __construct(){
        $this->middleware('ajax')->only('destroy');
    }

}
