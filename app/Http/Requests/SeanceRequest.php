<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->seance ? ',' . $this->seance->id : '';
        return [
            'hours' => 'required|string|max:10',
            'roomName_id' => 'required|string|max:20',
            'title_id' => 'required|string|max:50',
            
        ];
    }
}
