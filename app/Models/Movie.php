<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use SoftDeletes;
    // ce sont les champs remplissables
    protected $fillable = [
        'title', 'year', 'artist_id', 'image'
    ];

    public function director(){
        return $this->belongsto('App\Models\Artist');
    }
    //many to many
    public function actors(){
        return $this->belongsToMany('App\Models\Artist');
    }
    //one to many
    public function has_directed(){
        return $this->hasMany('App\Models\Seance');
    }
}