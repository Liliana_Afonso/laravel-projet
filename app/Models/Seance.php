<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
    protected $fillable = [
        'hours', 'title_id', 'roomName_id'
    ];
    //one to many
    public function director(){
        return $this->belongsTo('App\Models\Movie');
    }
    public function has_played(){
        return $this->belongsToMany('App\Models\Room')->withPivot('role_name');
    }
    public function actors(){
        return $this->belongsToMany('App\Models\Cinema');
    }
}
