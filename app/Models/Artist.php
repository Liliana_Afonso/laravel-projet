<?php

namespace App\Models;
use \App\Models\Movie;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Artist extends Model
{
    // ce sont les champs remplissables
    protected $fillable = [
        'name', 'firstname', 'birthdate', 'image'
    ];

    public function has_directed(){
        return $this->hasMany('App\Models\Movie');
    }

    //On a mis un pivot pour ajouter une info
    public function has_played(){
        return $this->belongsToMany('App\Models\Movie')->withPivot('role_name');
    }

    public function movies_played(){
        // Movie::where('artist_id','14')->first();
        Artist::where('id','14')->first();

        // DB::table('movies')->where('artist_id', 14);
        // DB::select('select * from artists where name = :name' , ['name' => 'Francis Ford']);


    }



}
