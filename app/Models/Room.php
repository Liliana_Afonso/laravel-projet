<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'roomName', 'numberSeats', 'cinema_id'
    ];

    public function actors(){
        return $this->belongsToMany('App\Models\Seance');
    }

    public function director(){
        return $this->belongsTo('App\Models\Cinema');
    }
}
