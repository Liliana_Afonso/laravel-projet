<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $fillable = [
        'name', 'street', 'postcode', 'city', 'country'
    ];
    public function has_directed(){
        return $this->hasMany('App\Models\Room');
    }

    public function is_played(){
        return $this->belongsToMany('App\Models\Seance')->withPivot('role_name');
    }
    
}
