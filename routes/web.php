<?php
use \App\Models\Movie;
use \App\Models\Artist;
use \App\Models\Seance;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome/');
    $movie_list = Movie::latest()->take(10)->get();
    $actor_list = Artist::latest()->take(10)->get();
    $seance_list = Seance::latest()->take(10)->get();

    return view('welcome.index')->with('movie_list', $movie_list ) ->with('actor_list', $actor_list) -> with('seance_list', $seance_list);
});

Route::resource ('artist', 'ArtistController');
Route::resource ('movie', 'MovieController');
Route::resource ('cinema', 'CinemaController');
Route::resource ('room', 'RoomController');
Route::resource ('seance', 'SeanceController');

// Route::get('profile', 'UserController@profile')->middleware('auth'); // redirige vers l'authentification si pas connecter ICI CEST UN EXEMPLE
// Route::resource ('movie', 'MovieController')->middleware('auth'); //--> oblige a être connecté si on veut accéder à la page movie


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// POUR LIER NOTRE BD ARTIST_MOVIE
// Route::get('/artist', function(){
//     $artist = new Artist;
//     $movie = Movie::all();

//     $artist->has_played()->attach($movie);
// });