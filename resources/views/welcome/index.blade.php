@extends('layouts.app')

@section('title', 'Laravel2')

@section ('title-content', 'Welcome to CinePopCorns')

@section('content')
    <h1>Welcome to CinePopCorns</h1>
    <div class="row">
        <div class="col-4">
            <table class="table table-striped table-light">
                <thead>
                    <tr>
                        <th scope="col">Last Movies</th>
                    </tr>
                </thead>
                <tbody >
                    @foreach($movie_list as $movie)
                        <tr>
                            <td>{{ $movie->title }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-4">
            <table class="table table-striped table-light">
                <thead>
                    <tr>
                    <th scope="col">Last Actors</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($actor_list as $artist)
                        <tr>
                            <td>{{ $artist->name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-4">
            <table class="table table-striped table-light">
                <thead>
                    <tr>
                        <th scope="col">Last seances</th>
                    </tr>
                </thead>
                <tbody >
                    @foreach($seance_list as $seance)
                        <tr>
                            <td>{{ $seance->hours }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

