@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">List of Rooms</h2>

<table class="table table-striped table-centered table-light">
    <thead>
        <tr>
            <th> {{ __('Room name') }} </th>
            {{-- <th> {{ __('Cinema name') }} </th> --}}
            <th> {{ __('Capacity room') }} </th>
            <th> {{ __('Actions') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach($rooms as $room)
        <tr>
            <td> {{ $room->roomName }} </td>
            {{-- @foreach($cinemas as $cinema)
                <td> {{ $cinema->name }} </td>
            @endforeach --}}
            <td> {{ $room->numberSeats }} </td>
            <td class="table-action"> 
                <a type="button" href="{{ route( 'room.edit', $room->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Edit room') {{ $room->roomName}}">
                    Edit
                </a>
                <a type="button" href="{{ route( 'room.destroy', $room->id) }}" class="btn btn-sm btn-delete"
                    data-toggle="tooltip" title="@lang('Delete room') {{ $room->roomName}}">
                    Remove
                </a>
            </td> 
        </tr>
    @endforeach
    </tbody>
</table>
<div class="example">
{{ $rooms->appends(request()->except('page'))->links() }}
</div>
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<a href="/room/create" class="btn btn-light float-right"> Add Room </a>


<script>
    // pour ajouter partout des en tete csrf (verrifier que le formulaire contient des bons elements)
$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
})

$(document).on('click', '.btn-delete', function(){
    let button = $(this);
    $.ajax ({
        url: button.attr('href'),
        type: 'DELETE'
    }).done(function(){
        button.closest('tr').remove();
    });
    return false;
});

</script>

@endsection
