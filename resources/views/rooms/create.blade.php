@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">Creation</h2>

<form method="POST" action="{{ route('room.store') }}">
    {{ csrf_field() }}
    <div class="form-row">
    <p class="form-group col-lg-6">
        <label for="roomName" class="float-left"> Room name </label>
        <input type="text" class="form-control" name="roomName" id="roomName" value="" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="numberSeats" class="float-left"> Capacity room</label>
        <input type="number" class="form-control" name="numberSeats" id="numberSeats" value="" min="50" max="400" required />
    </p>

   <p class="form-group col-lg-6">
        <label for="cinema_id" class="float-left"> Cinema</label>
        <select class="form-control" name="cinema_id" id="cinema_id">
            @foreach($cinemas as $cinema)
                <option value="{{ $cinema->id }}"> {{ $cinema->name }} </option>
            @endforeach
        </select>
    </p>
    </div>
    <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
    <button type="submit" class="btn btn-light float-right"> Save </button>
</form>
@endsection