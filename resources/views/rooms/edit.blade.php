@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">Edit</h2>

<form method="POST" action="{{ route('room.update',$room->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="form-row">
    <p class="form-group col-lg-6">
        <label for="roomName" class="float-left"> Room name </label>
        <input type="text" class="form-control" name="roomName" id="roomName" value="{{ $room->roomName }}" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="numberSeats" class="float-left"> Capacity room</label>
        <input type="number" class="form-control" name="numberSeats" id="numberSeats" value="{{ $room->numberSeats }}" min="50" max="400" required />
    </p>
    </div>
</form>
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<button type="submit" class="btn btn-light float-right"> Create </button>
@endsection