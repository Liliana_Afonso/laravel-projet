@extends('layouts.app')

@section('content')
<h2 class="mt-5 mb-5">Creation</h2>

<form method="POST" action="{{ route('cinema.store') }}">
    {{ csrf_field() }}
    <div class="form-row">
    <p class="form-group col-lg-6">
        <label for="name" class="float-left"> Name of Cinema </label>
        <input type="text" class="form-control" name="name" id="name" value="" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="street" class="float-left"> Street </label>
        <input type="text" class="form-control" name="street" id="street" value="" />
    </p>
    <p class="form-group col-lg-6">
        <label for="postcode" class="float-left"> Postcode </label>
        <input type="number" class="form-control" name="postcode" id="postcode" value="" /> 
    </p>
    <p class="form-group col-lg-6">
        <label for="city" class="float-left"> City </label>
        <input type="text" class="form-control" name="city" id="city" value="" />
    </p>    <p class="form-group col-lg-6">
        <label for="country" class="float-left"> Country </label>
        <input type="text" class="form-control" name="country" id="country" value="" />
    </p>
    </div>
    <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
    <button type="submit" class="btn btn-light float-right"> Save </button>
</form>
@endsection