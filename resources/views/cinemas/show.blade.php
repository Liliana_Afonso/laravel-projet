@extends('layouts.app')
@section('content')

    <h1 >{{ $cinema->name }}</h1>
    <h3> Adresse:</h3>
    
    <p>{{ $cinema->street }}</p>
    <p>{{ $cinema->postcode }}</p>
     <p>{{ $cinema->city }}</p>
    <p>{{ $cinema->country}}</p>


    <h3>Rooms: </h3>
    <ul class="list-group list-group-flush col-6 mx-auto">
        {{-- On voulait également afficher que les room correspondante au cinema mais on n'a pas réussit également --}}
        @foreach($rooms as $room)
        <li value="{{ $room->id }}" class="list-group-item"> {{ $room->roomName }} {{ $room->numberSeats }} </li>
    @endforeach
    </ul>
    <h3>Movies: </h3>
    {{-- Ne marche pas on n'a pas réussir à faire le lien en many to many --}}
    {{-- <ul>
    @foreach($seances as $seance)
        <li value="{{ $seance->id}}"> {{ $seance->title_id }} </li>
    @endforeach
    </ul> --}}
    {{-- <h3>seance: </h3>
    <ul>
    @foreach($seances as $seance)
        <li value="{{ $seance->id}}"> {{ $seance->hours }} </li>
    @endforeach
    </ul>  --}}
   
       
    <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
@endsection