@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">List of Cinemas</h2>

<table class="table table-striped table-centered table-light">
    <thead>
        <tr>
            <th> {{ __('Name') }} </th>
            <th> {{ __('Street') }} </th>
            <th> {{ __('Postcode') }} </th>
            <th> {{ __('City') }} </th>
            <th> {{ __('Country') }} </th>
            <th> {{ __('Actions') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach($cinemas as $cinema)
        <tr>
            <td> {{ $cinema->name }} </td>
            <td> {{ $cinema->street }} </td>
            <td> {{ $cinema->postcode}} </td>
            <td> {{ $cinema->city}} </td>
            <td> {{ $cinema->country}} </td>
            <td class="table-action"> 
                <a type="button" href="{{ route( 'cinema.edit', $cinema->id)}}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Edit cinema') {{ $cinema->name}}">
                    Edit
                </a>
                <a type="button" href="{{ route( 'cinema.destroy', $cinema->id) }}" class="btn btn-sm btn-delete"
                    data-toggle="tooltip" title="@lang('Delete cinema') {{ $cinema->name}}">
                    Remove
                </a>
                <a type="button" href="{{ route( 'cinema.show', $cinema->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Show cinema') {{ $cinema->name}}">
                    Show
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $cinemas->appends(request()->except('page'))->links() }}
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<a href="/cinema/create" class="btn btn-light float-right"> Add Cinema </a>

<script>
    // pour ajouter partout des en tete csrf (verrifier que le formulaire contient des bons elements)
$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
})

$(document).on('click', '.btn-delete', function(){
    let button = $(this);
    $.ajax ({
        url: button.attr('href'),
        type: 'DELETE'
    }).done(function(){
        button.closest('tr').remove();
    });
    return false;
});

</script>

@endsection

