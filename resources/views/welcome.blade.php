@extends('layouts.app')

@section('title', 'Laravel2')

@section ('title-content', 'Welcome to CinePopCorns')

@section ('menu')
    <a href="artist">Artists List</a>
    <a href="cinema">Cinemas List</a>
    <a href="movie">Movies List</a>
    <a href="room">Rooms List</a>
    <a href="seance">Seances List</a>
@endsection

@section('content')
    <h1>Welcome to CinePopCorns</h1>
    @foreach($movies as $movie)
        <li> {{ $movie->title }} {{ $movie->year }} </li>
    @endforeach
@endsection

