@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">List of Seances</h2>

<table class="table table-striped table-centered table-light">
    <thead>
        <tr>
           
            <th> {{ __('Title') }} </th>
            <th> {{ __('Room Name') }} </th>
            <th> {{ __('Hours') }} </th>
            <th> {{ __('Actions') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach($seances as $seance)
        <tr>
            <td> {{ $seance->title_id }} </td>
            <td> {{ $seance->roomName_id }} </td>
            <td> {{ $seance->hours }} </td>
             <td class="table-action"> 
                <a type="button" href="{{ route( 'seance.edit', $seance->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Edit seance') {{ $seance->title_id}}">
                    Edit
                </a>

                <a type="button" href="{{ route( 'seance.destroy', $seance->id) }}" class="btn btn-sm btn-delete"
                    data-toggle="tooltip" title="@lang('Delete seance') {{  $seance->title_id}}">
                    Remove
                </a>
                {{-- <a type="button" href="{{ route( 'seance.show', $seance->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Show seance') {{ $seance->title}}">
                    Show
                </a> --}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $seances->appends(request()->except('page'))->links() }}
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<a href="/seance/create" class="btn btn-light float-right"> Add Seance </a>

<script>
    // pour ajouter partout des en tete csrf (verrifier que le formulaire contient des bons elements)
$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
})

$(document).on('click', '.btn-delete', function(){
    let button = $(this);
    $.ajax ({
        url: button.attr('href'),
        type: 'DELETE'
    }).done(function(){
        button.closest('tr').remove();
    });
    return false;
});

</script>

@endsection

