@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">Creation</h2>

<form method="POST" action="{{ route('seance.store') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-row"> 
    <p class="form-group col-lg-6">
        <label for="title_id" class="float-left"> Movie Name </label>
        <select class="form-control" name="title_id" id="title_id">
            @foreach($movies as $movie)
                <option value="{{ $movie->id }}"> {{ $movie->title}} </option>
            @endforeach
        </select>
    </p>
    <p class="form-group col-lg-6">
        <label for="roomName_id" class="float-left"> Room </label>
        <select class="form-control" name="roomName_id" id="roomName_id">
            @foreach($rooms as $room)
                <option value="{{ $room->id }}"> {{ $room->roomName}} </option>
            @endforeach
        </select>

    </p>
    <p class="form-group col-lg-6">
      <label for="hours" class="float-left"> Hours </label>
      <input type="text" class="form-control" name="hours" id="hours" value="" required />
  </p>
    </div>
  <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
    <button type="submit" class="btn btn-light float-right"> Save </button>
</form>
@endsection