<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Boostrap MODIFIER-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        
        <!-- Styles -->
        <style>
            html, body {
                background-color: #FBF4EB;
                color:#000!important;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            h1{
                margin:100px  0 50px;
                padding:10px;
                /* font-weight: 600; */
                text-transform: uppercase;
                }
            a {color:#000}
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                width:80%;
            }

            .title {
                font-size: 84px;
            }
            .links {
                /* background:#fd3C06 ; */
                background: white;
                color:#fff!important;
                padding: 10px;
                }
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .pagination > li > a
            {
                color: #000;
            }
            .page-item.active .page-link {
                background: #DA3721;
                border-color: #DA3721;
            }

            .pagination > li > a:focus,
            .pagination > li > a:hover,
            .pagination > li > span:focus,
            .pagination > li > span:hover{
                color: #5a5a5a;
            }
            .table-striped tbody tr:nth-of-type(odd) {
                background: rgba(218, 55, 33, 1);
                background: white;
            }
            .btn-light,
            h1 {
                color: white;
                background: rgba(218, 55, 33, 1);
            }
        </style>
    </head>
    <body>
            <div class="navbar-menu ">
                <div class="navbar-start links">
                    <a href="{{ url('/') }}"> <img src="{{url('/Images/logo.png')}}" alt="logo-cinePopCorn" width="70" /> </a>
                    <a href="{{ url('/artist') }}">Artists List</a>
                    <a href="{{ url('/cinema') }}" >Cinemas List</a>
                    <a href="{{ url('/movie') }}">Movies List</a>
                    <a href="{{ url('/room') }}" >Rooms List</a>
                    <a href="{{ url('/seance') }}">Seances List</a>
                </div>
                <div class="navbar-end">
                    @if (Route::has('login'))
                        <div class="top-right links">
                            @auth
                                <a href="{{ url('/home') }}">Sign out</a>
                            @else
                                <a href="{{ route('login') }}">Login</a>

                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}">Register</a>
                                @endif
                            @endauth
                        </div>
                    @endif
                </div>
            </div>

        <div class="flex-center position-ref">

            @if (session('ok'))
            <div class="">
                <div class="alert alert-dismissible alert-success fade show fixed-top" role="alert">
                {{ session('ok') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    @yield('text-content')
                </div>
                    @yield('content')
               
            </div>
        </div>
    </body>
</html>
