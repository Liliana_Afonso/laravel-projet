@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">Edit</h2>

<form method="POST" action="{{ route('artist.update', $artist->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="form-row">

        <p class="form-group col-lg-6">
            <label for="name" class="float-left"> Name </label>
            <input type="text" class="form-control" name="name" id="name" value="{{ $artist->name}}" required />
        </p>
        <p class="form-group col-lg-6">
            <label for="firstname" class="float-left"> first name </label>
            <input type="text" class="form-control" name="firstname" id="firstname" value="{{ $artist->firstname}}" required />
        </p>
    </div>
</form>
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<button type="submit" class="btn btn-light float-right"> Save </button>
@endsection