@extends('layouts.app')
@section('content')
    <div class="text-center">
        <img src="{{ '/uploads/profiles/profile_' . $artist->id . '.png' }}" class="rounded" alt="" height="300">
    </div>
    <h2 >{{ $artist->name}} {{ $artist->firstname}}</h2>
    <p>{{ $artist->birthdate}}</p>
    <h3>Movies: </h3>
    
    <ul class="list-group list-group-flush col-6 mx-auto">
        {{-- @foreach($artist_movie->$movies as $movie)  --> CAS SI LE TABLE MANY TO MANY MARCHERAIT --}} 
        @foreach($movies as $movie)
            <li value="{{ $movie->id}}" class="list-group-item"> {{ $movie->title }} {{ $movie->year }} </li>
        @endforeach
    </ul>
       
    <a href="{{ url()->previous() }}" class="btn btn-light float-center mt-5 mb-5"> Back </a>

@endsection