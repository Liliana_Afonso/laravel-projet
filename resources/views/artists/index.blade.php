@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">List of Artists</h2>

<table class="table table-striped table-centered table-light">
    <thead>
        <tr>
            <th> {{ __('Name') }} </th>
            <th> {{ __('Firstname') }} </th>
            <th> {{ __('Actions') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach($artists as $artist)
        <tr>
            <td> {{ $artist->name }} </td>
            <td> {{ $artist->firstname }} </td>
            <td class="table-action"> 
                <a type="button" href="{{ route( 'artist.edit', $artist->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Edit artist') {{ $artist->name}}">
                    Edit
                </a>
                <a type="button" href="{{ route( 'artist.destroy', $artist->id) }}" class="btn btn-sm btn-delete"
                    data-toggle="tooltip" title="@lang('Delete artist') {{ $artist->name}}">
                    Remove
                </a> <a type="button" href="{{ route( 'artist.show', $artist->id) }}" class="btn btn-sm "
                    data-toggle="tooltip" title="@lang('Show artist') {{ $artist->name}}">
                    Show
                </a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $artists->appends(request()->except('page'))->links() }}
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<a href="/artist/create" class="btn btn-light float-right"> Add Artist</a>

<script>
    // pour ajouter partout des en tete csrf (verrifier que le formulaire contient des bons elements)
$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
})

$(document).on('click', '.btn-delete', function(){
    let button = $(this);
    $.ajax ({
        url: button.attr('href'),
        type: 'DELETE'
    }).done(function(){
        button.closest('tr').remove();
    });
    return false;
});

</script>

@endsection

<?php
# /*MODIFIER*/
?>