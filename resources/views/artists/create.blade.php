@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">Creation</h2>

<form method="POST" action="{{ route('artist.store') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-row">
    <p class="form-group col-lg-6">
        <label for="name" class="float-left"> Name </label>
        <input type="text" class="form-control" name="name" id="name" value="" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="firstname" class="float-left"> first name </label>
        <input type="text" class="form-control" name="firstname" id="firstname" value="" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="image" class="float-left" >Choose a image in png format</label>
        <input type="file" class="form-control-file" id="image" name="image" accept=".png,.jpeg" >
    </p>
    </div>
    <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
    <button type="submit" class="btn btn-light float-right"> Save </button>
</form>
@endsection