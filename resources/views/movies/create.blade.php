@extends('layouts.app')

@section('content')
<h2 class="mt-5 mb-5">Creation</h2>

<form method="POST" action="{{ route('movie.store') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-row">
    <p class="form-group col-lg-6">
        <label for="title" class="float-left"> Title </label>
        <input type="text" class="form-control" name="title" id="title" value="" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="year" class="float-left"> Year </label>
        <input type="number" class="form-control" name="year" id="year" value="" min="1900" max="2050" />
    </p>
    <p class="form-group col-lg-6">
        <label for="artist_id" class="float-left"> Artist </label>
        <select class="form-control" name="artist_id" id="artist_id">
            @foreach($artists as $artist)
                <option value="{{ $artist->id }}"> {{ $artist->name }} {{ $artist->firstname }} </option>
            @endforeach
        </select>
    </p>
    <p class="form-group col-lg-6">
        <label for="image" class="float-left" >Choose a image in png format</label>
        <input type="file" class="form-control-file" id="image" name="image" accept=".png" >
    </p>
    </div>
    <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
    <button type="submit" class="btn btn-light float-right"> Save </button>
</form>
@endsection