@extends('layouts.app')
@section('content')

    <div class="text-center">
        <img src="{{ '/uploads/posters/poster_' . $movie->id . '.png' }}" class="rounded" height="300" alt="">
    </div>
    <h2 >{{ $movie->title }}</h2>
    <p>Year: {{ $movie->year }}</p>
    <p>Artists: </p>
    
    <ul class="list-group list-group-flush col-6 mx-auto">
        {{-- @foreach($actors->$artists as $artist)  --> CAS SI LE TABLE MANY TO MANY MARCHERAIT --}} 
        @foreach($artists as $artist)
            <li value="{{ $artist->id }}" class="list-group-item"> {{ $artist->name }} {{ $artist->firstname }} </li>
        @endforeach
    </ul>
 
<a href="{{ url()->previous() }}" class="btn btn-light float-center mt-5 mb-5"> Back </a>

@endsection