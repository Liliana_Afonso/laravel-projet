@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">List of Movies</h2>

<table class="table table-striped table-centered table-light">
    <thead>
        <tr>
            <th> {{ __('Title') }} </th>
            <th> {{ __('Year') }} </th>
            <th> {{ __('Actions') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach($movies as $movie)
        <tr>
            <td> {{ $movie->title }} </td>
            <td> {{ $movie->year }} </td>
            <td class="table-action"> 
                <a type="button" href="{{ route( 'movie.edit', $movie->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Edit movie') {{ $movie->title}}">
                    Edit
                </a>
                <a type="button" href="{{ route( 'movie.destroy', $movie->id) }}" class="btn btn-sm btn-delete"
                    data-toggle="tooltip" title="@lang('Delete movie') {{ $movie->title}}">
                    Remove
                </a>
                <a type="button" href="{{ route( 'movie.show', $movie->id) }}" class="btn btn-sm"
                    data-toggle="tooltip" title="@lang('Show movie') {{ $movie->title}}">
                    Show
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $movies->appends(request()->except('page'))->links() }}
<a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>
<a href="/movie/create" class="btn btn-light float-right"> Add Movie </a>


<script>
    // pour ajouter partout des en tete csrf (verrifier que le formulaire contient des bons elements)
$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
})

$(document).on('click', '.btn-delete', function(){
    let button = $(this);
    $.ajax ({
        url: button.attr('href'),
        type: 'DELETE'
    }).done(function(){
        button.closest('tr').remove();
    });
    return false;
});

</script>

@endsection

<?php
# /*MODIFIER*/
?>