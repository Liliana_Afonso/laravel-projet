@extends('layouts.app')
@section('content')
<h2 class="mt-5 mb-5">Edit</h2>

<form method="POST" action="{{ route('movie.update',$movie->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="form-row">
    <p class="form-group col-lg-6">
        <label for="title" class="float-left"> Title </label>
        <input type="text" class="form-control" name="title" id="title" value="{{ $movie->title }}" required />
    </p>
    <p class="form-group col-lg-6">
        <label for="year" class="float-left"> Year </label>
        <input type="number" class="form-control" name="year" id="year" value="{{ $movie->year }}" min="1900" max="2050" />
    </p>
    <p class="form-group col-lg-6">
        <label for="artist_id" class="float-left"> Artist ID </label>
        <select class="form-control" name="artist_id" id="artist_id">
            @foreach($artists as $artist)
                <option value="{{ $artist->id }}"> {{ $artist->name }} {{ $artist->firstname }} </option>
            @endforeach
        </select>
    </p>
    </div>
    <a href="{{ url()->previous() }}" class="btn btn-light float-left"> Back </a>

    <button type="submit" class="btn btn-light float-right"> Save </button>
    

</form>

@endsection